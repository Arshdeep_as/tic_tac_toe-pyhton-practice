# Tic Tac Toe
import random

def drawBoard(board):
# Function to draw the board that was passed on the screen
# "board" basically is a list of 10 strings representing the board, i am ignoring the index 0 for better readability
  print('   |   |   ')
  print(' ' + board[7] + ' | ' + board[8] + ' | ' + board[9])
  print('   |   |   ')
  print('-----------')
  print('   |   |   ')
  print(' ' + board[4] + ' | ' + board[5] + ' | ' + board[6])
  print('   |   |   ')
  print('-----------')
  print('   |   |   ')
  print(' ' + board[1] + ' | ' + board[2] + ' | ' + board[3])
  print('   |   |   ')

def getPlayerSymbol():
# Let the player decides which symbol he wants to be, wheather X or O.
# Returns a list with the player’s symbol as the first item, and the computer's symbol as the second.
  letter = ''
  while not (letter == 'X' or letter == 'O'):
    print('Do you want to be X or O?')
    letter = input().upper()
    # the first element in the list is the player’s symbol, the second is the computer's symbol.

  if letter == 'X':
    return ['X', 'O']
  else:
    return ['O', 'X']

def firstTurn():
  # This function randomly returns who goes first, The player or the Computer.
  if random.randint(0, 1) == 0:
    return 'computer'
  else:
    return 'player'

def playAgain():
  # This function asks player if he wants to play abain or not.
  print('Do you want to play again? (yes or no)')
  return input().lower().startswith('y')

def makeMove(board, letter, move):
  board[move] = letter

def isWinner(bo, le):
  # This function accepts two parameters, a board and a player’s symbol, returns True if that player has won.
  return ((bo[7] == le and bo[8] == le and bo[9] == le) or # across the top
  (bo[4] == le and bo[5] == le and bo[6] == le) or # across the middle
  (bo[1] == le and bo[2] == le and bo[3] == le) or # across the bottom
  (bo[7] == le and bo[4] == le and bo[1] == le) or # down the left side
  (bo[8] == le and bo[5] == le and bo[2] == le) or # down the middle
  (bo[9] == le and bo[6] == le and bo[3] == le) or # down the right side
  (bo[7] == le and bo[5] == le and bo[3] == le) or # diagonal
  (bo[9] == le and bo[5] == le and bo[1] == le)) # diagonal

def getBoardCopy(board):
  # This function makes a copy of the board list and return it.
  dupeBoard = []
  for i in board:
    dupeBoard.append(i)

  return dupeBoard

def isSpaceFree(board, move):
  # This function checks if the space on board is empty. If empty Return true.
  return board[move] == ' '

def getPlayerMove(board):
  # This function get the player move.
  move = ' '
  while move not in '1 2 3 4 5 6 7 8 9'.split() or not isSpaceFree(board, int(move)):
    print('What is your next move? (1-9)')
    move = input()
  return int(move)

def chooseRandomMoveFromList(board, movesList):
  # Returns a valid move from the passed list on the passed board.
  # Returns None if there is no valid move.
  possibleMoves = []
  for i in movesList:
    if isSpaceFree(board, i):
      possibleMoves.append(i)

    if len(possibleMoves) != 0:
      return random.choice(possibleMoves)
    else:
      return None

def getComputerMove(board, computerLetter):
  # Given a board and the computer's symbol, determine where to move and return that move.
  if computerLetter == 'X':
    playerLetter = 'O'
  else:
    playerLetter = 'X'

  # Algorithm for our Tic Tac Toe AI:
  # First step is, check if computer can win in the next move
  for i in range(1, 10):
    copy = getBoardCopy(board)
    if isSpaceFree(copy, i):
      makeMove(copy, computerLetter, i)
      if isWinner(copy, computerLetter):
        return i

  # Second Step is, Check if the player can win on their next move, if he can then block them.
  for i in range(1, 10):
    copy = getBoardCopy(board)
    if isSpaceFree(copy, i):
      makeMove(copy, playerLetter, i)
      if isWinner(copy, playerLetter):
        return i

  # Third Step is, If computer and player both can't win on there next movrself.
  # Then try to take one of the corners, if they are free.
  move = chooseRandomMoveFromList(board, [1, 3, 7, 9])
  if move != None:
    return move

  # Fourth Step is,tTry to take the center, if it is free.
  if isSpaceFree(board, 5):
    return 5
  # Fifth Step is, try to occupy on one of the sides.
  return chooseRandomMoveFromList(board, [2, 4, 6, 8])

def isBoardFull(board):
  # This function checks id the board is full or not.
  for i in range(1, 10):
    if isSpaceFree(board, i):
      return False
  return True

# Game start from here.
print('Welcome to Tic Tac Toe!')

while True:
  # Reset the board
  theBoard = [' '] * 10
  playerLetter, computerLetter = getPlayerSymbol()
  turn = firstTurn()
  print('The ' + turn + ' will go first.')
  gameIsPlaying = True

  while gameIsPlaying:
    if turn == 'player':
      # Player’s turn.
      drawBoard(theBoard)
      move = getPlayerMove(theBoard)
      makeMove(theBoard, playerLetter, move)

      if isWinner(theBoard, playerLetter):
        drawBoard(theBoard)
        print('Hooray! You have won the game!')
        gameIsPlaying = False
      else:
        if isBoardFull(theBoard):
          drawBoard(theBoard)
          print('The game is a tie!')
          break
        else:
          turn = 'computer'
    else:
      # Computer’s turn.
      move = getComputerMove(theBoard, computerLetter)
      makeMove(theBoard, computerLetter, move)

      if isWinner(theBoard, computerLetter):
        drawBoard(theBoard)
        print('The computer has beaten you! You lose.')
        gameIsPlaying = False
      else:
        if isBoardFull(theBoard):
          drawBoard(theBoard)
          print('The game is a tie!')
          break
        else:
          turn = 'player'
  if not playAgain():
    break
